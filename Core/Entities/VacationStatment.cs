﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entities
{
    public class VacationStatment : BaseEntity
    {
        public int EmployeeId { get; set; }
        public VacatyionType VacatyionType { get; set; }
        public int VacatyionTypeId { get; set; }
        public DateTime VacationStart { get; set; }
        public DateTime VacationEnd { get; set; }
        public List<VacationDay> VacationDays { get; set; }
        public int TotalVacationDays { get; set; }
        public StatmentStatus Status { get; set; }
        public enum StatmentStatus
        {
            Creation,
            OnMatching,
            Agreed,
            Cancel,
            Close
        }

    }
}
