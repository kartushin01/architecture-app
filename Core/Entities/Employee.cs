﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entities
{
    public class Employee : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Department Departament { get; set; }
        public int DepartamentId { get; set; }
        public Position CurrentPosition { get; set; }
        public List<PositionHistory> PositionsHistory { get; set;}
        public List<VacationDay> TotalDayOnVacatron { get; set; }
    }
}
