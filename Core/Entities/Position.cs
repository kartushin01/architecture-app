﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entities
{
    public class Position : BaseEntity
    {
        public string Name { get; set; }
        public int DepartamentId { get; set; }
        public Department Departament { get; set; }

    }
}
