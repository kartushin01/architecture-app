﻿using System;

namespace Core.Entities
{
    public class VacationDay : BaseEntity
    {
        public int VacationStatmentId { get; set; }
        public DateTime DayOnVacation { get; set; }
    }
}