﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entities
{
    public class Company : BaseEntity
    {
        public string Name { get; set; }
        public List<Department> Departments { get; set; }

    }
}
