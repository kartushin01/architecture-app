﻿using System;

namespace Core.Entities
{
    public class PositionHistory
    {
        public Position Position { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
    }
}