﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entities
{
    public class Department : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Company Company { get; set; }
        public int CompanyId { get; set; }
        public List<Employee> Employees { get; set; }
    }
}
