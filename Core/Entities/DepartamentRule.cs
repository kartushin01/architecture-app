﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entities
{
    public class DepartamentRule
    {
        public int TotalVacationDays { get; set; }
        public int SameTimePeopleOnVacation { get; set; }
    }
}
